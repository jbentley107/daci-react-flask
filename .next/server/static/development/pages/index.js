module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/AddConnectionForm/AddConnectionForm.json":
/*!*************************************************************!*\
  !*** ./components/AddConnectionForm/AddConnectionForm.json ***!
  \*************************************************************/
/*! exports provided: title, type, required, properties, form, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"title\":\"\",\"type\":\"object\",\"required\":[\"title\",\"type\"],\"properties\":{\"title\":{\"type\":\"string\",\"title\":\"Logical connection name\",\"placeholder\":\"Connection Name\"},\"comments\":{\"type\":\"string\",\"title\":\"Comments\",\"placeholder\":\"Enter your comments\"},\"type\":{\"type\":\"string\",\"title\":\"Connection Type\",\"placeholder\":\"Select Connection\",\"enum\":[\"PostgreSQL\",\"Redshift\"]},\"host\":{\"type\":\"string\",\"title\":\"Host\",\"placeholder\":\"Enter host\"},\"username\":{\"type\":\"string\",\"title\":\"User name\",\"placeholder\":\"Enter User name\"},\"password\":{\"type\":\"string\",\"title\":\"Password\",\"placeholder\":\"Create password\"},\"database\":{\"type\":\"string\",\"title\":\"Database\",\"placeholder\":\"Enter database\"},\"URL\":{\"type\":\"string\",\"title\":\"URL\",\"placeholder\":\"Enter URL\"}},\"form\":[{\"key\":\"comments\",\"type\":\"textarea\"},{\"key\":\"password\",\"type\":\"password\"}]}");

/***/ }),

/***/ "./components/AddConnectionForm/AddConnectionForm.jsx":
/*!************************************************************!*\
  !*** ./components/AddConnectionForm/AddConnectionForm.jsx ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_jsonschema_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-jsonschema-form */ "react-jsonschema-form");
/* harmony import */ var react_jsonschema_form__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_jsonschema_form__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ConnectionButton_ConnectionButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ConnectionButton/ConnectionButton */ "./components/ConnectionButton/ConnectionButton.jsx");
/* harmony import */ var _ConnectionAddButton_ConnectionAddButton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../ConnectionAddButton/ConnectionAddButton */ "./components/ConnectionAddButton/ConnectionAddButton.jsx");
/* harmony import */ var _ConnectionList_ConnectionList__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ConnectionList/ConnectionList */ "./components/ConnectionList/ConnectionList.jsx");
/* harmony import */ var _AddConnectionForm_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./AddConnectionForm.json */ "./components/AddConnectionForm/AddConnectionForm.json");
var _AddConnectionForm_json__WEBPACK_IMPORTED_MODULE_6___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./AddConnectionForm.json */ "./components/AddConnectionForm/AddConnectionForm.json", 1);
/**
 * @file AddConnectionForm.js
 */
// Import dependencies








const AddConnectionForm = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props; // Set up our variables that will control various states

  const [showConnectionList, setShowConnectionList] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false); // State to show/hide connections

  const [isTestButtonDisabled, setIsTestButtonDisabled] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  const [isSaveButtonDisabled, setIsSaveButtonDisabled] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  const [formDataState, setFormDataState] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}); // Use this object to add special attributes to form fields
  // Examples: classNames, id, or special form element types

  const uiSchema = {
    comments: {
      "ui:widget": "textarea",
      classNames: "form-control",
      value: " "
    }
  };
  let currentFormData = {};
  let [// The array we will be passing into the <ConnectionList />
  returnedFormData = [], // The function that actually updates the <ConnectionList /> state
  setFormData] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]); // the initial state of the <ConnectionList />

  function onChange({
    formData
  }, e) {
    // Store the live form data in a variable (pass by value)
    currentFormData = JSON.parse(JSON.stringify(formData)); // Check if a value has been entered into the critical fields

    if (formData.title && formData.comments && formData.type && isTestButtonDisabled) {
      setIsTestButtonDisabled(false); // React re-renders the form here

      setIsSaveButtonDisabled(false); // Update the form data state with the values pre-render

      setFormDataState(JSON.parse(JSON.stringify(currentFormData)));
    }
  } // This function runs when the Form is submitted
  // It returns some form data, and sets the state for the ConnectionList


  function onSubmit({
    formData
  }, e) {
    // We must create a copy of the array because of JS pass by reference
    let formDataArray = returnedFormData.slice(); // Add the latest form data to the array of form data objects

    formDataArray.push(formData); // Pass the updated array of form data objects to the 
    // ComponentList's state

    setFormData(formDataArray);
    setShowConnectionList(true); // Reset the field states and the buttons

    setIsTestButtonDisabled(true);
    setIsSaveButtonDisabled(true);
    setFormDataState({});
  }

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    id: "defaultConnection",
    className: `add-connection-form add-connection-form`
  }, showConnectionList ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ConnectionList_ConnectionList__WEBPACK_IMPORTED_MODULE_5__["default"], {
    formData: returnedFormData
  }) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `connection-header connection-header bg-top-row-background md:h-16 md:w-full shadow flex align-middle`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ConnectionAddButton_ConnectionAddButton__WEBPACK_IMPORTED_MODULE_4__["default"], {
    buttonText: "Add Connection",
    className: 'w-3/5'
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `connection-button-list inline text-center align-middle md:h-16 md:mr-3 w-2/5 m-3`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ConnectionButton_ConnectionButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
    disabled: isTestButtonDisabled,
    form: "add-connection-form",
    text: "Test",
    type: "button",
    hierarchy: "secondary"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ConnectionButton_ConnectionButton__WEBPACK_IMPORTED_MODULE_3__["default"], {
    disabled: isSaveButtonDisabled,
    form: "add-connection-form",
    text: "Save",
    type: "submit",
    hierarchy: "primary",
    id: 'add_connection_submit'
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "inline-block border-solid h-auto md:mt-6 text-sm w-full"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "inline md:ml-4 md:mt-2 text-form-text"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_jsonschema_form__WEBPACK_IMPORTED_MODULE_2___default.a, {
    id: "add-connection-form",
    className: "react-jsonschema-form pl-10 w-3/5",
    schema: _AddConnectionForm_json__WEBPACK_IMPORTED_MODULE_6__,
    uiSchema: uiSchema,
    onChange: onChange,
    onSubmit: onSubmit,
    formData: formDataState
  }))));
};

AddConnectionForm.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
AddConnectionForm.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (AddConnectionForm);

/***/ }),

/***/ "./components/ConnectionAddButton/ConnectionAddButton.jsx":
/*!****************************************************************!*\
  !*** ./components/ConnectionAddButton/ConnectionAddButton.jsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/**
 * @file ConnectionAddButton.js
 */



const ConnectionAddButton = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
    buttonText
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `connection-add-button connection-add-button--${variant} ${className} md:h-16 inline align-middle`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "align-middle inline md:p-3 md:w-16 md:h-auto",
    id: "connections-add-row-icon",
    src: "../../../static/add_in_circle.png",
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    id: "add-connection-text",
    className: "align-middle leading-none my-2 inline align-middle"
  }, buttonText));
};

ConnectionAddButton.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,
  buttonText: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string
};
ConnectionAddButton.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
  buttonText: 'Default Text'
};
/* harmony default export */ __webpack_exports__["default"] = (ConnectionAddButton);

/***/ }),

/***/ "./components/ConnectionButton/ConnectionButton.jsx":
/*!**********************************************************!*\
  !*** ./components/ConnectionButton/ConnectionButton.jsx ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/**
 * @file ConnectionButton.js
 */
// Import dependencies


let additionalClasses = ""; // Function here to determine what type of button needs to be loaded

function determineAdditionalClasses(hierarchy) {
  // If a primary button is chosen, add the classes that are unique to that button
  if (hierarchy == 'primary') {
    additionalClasses = "ml-3 py-1 md:w-20 md:h-auto bg-primary-button-background text-white rounded-full border-primary-button-background text-center duration-500 font-display hover:cursor-pointer hover:shadow";
  } else {
    additionalClasses = "border-secondary-button-background text-secondary-button-background text-white hover:secondary-button-background-hover";
  }
}

const ConnectionButton = props => {
  const {
    disabled,
    form,
    type,
    text,
    hierarchy,
    tagName: Tag,
    className,
    id,
    variant,
    children
  } = props; // Determine what classes to add to a button

  determineAdditionalClasses(hierarchy);

  if (!disabled) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
      form: form,
      id: id,
      type: type,
      className: `connection-button connection-button--${variant} ${className} ml-3 py-1 md:w-20 md:h-auto rounded-full border border-solid text-center duration-500 font-display hover:cursor-pointer` + ` ` + additionalClasses
    }, text);
  } else {
    // Render a button in disabled state
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
      form: form,
      id: id,
      type: type,
      className: `connection-button connection-button--${variant} ${className} ml-3 py-1 md:w-20 md:h-auto rounded-full border border-solid text-center duration-500 font-display hover:cursor-pointer opacity-25 cursor-default` + ` ` + additionalClasses
    }, text);
  }
};

ConnectionButton.propTypes = {
  form: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  type: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  text: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  hierarchy: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  id: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.node
};
ConnectionButton.defaultProps = {
  form: 'form',
  type: 'button',
  text: 'Button',
  tagName: 'div',
  className: '',
  id: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (ConnectionButton);

/***/ }),

/***/ "./components/ConnectionList/ConnectionList.json":
/*!*******************************************************!*\
  !*** ./components/ConnectionList/ConnectionList.json ***!
  \*******************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"title\":\"Connection name\"},{\"title\":\"Assigned connection to EPUs\"},{\"title\":\"Run Frequency\"}]");

/***/ }),

/***/ "./components/ConnectionList/ConnectionList.jsx":
/*!******************************************************!*\
  !*** ./components/ConnectionList/ConnectionList.jsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ConnectionList_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ConnectionList.json */ "./components/ConnectionList/ConnectionList.json");
var _ConnectionList_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./ConnectionList.json */ "./components/ConnectionList/ConnectionList.json", 1);
/**
 * @file ConnectionList.js
 */




const ConnectionList = props => {
  const {
    formData,
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Tag, {
    className: `connection-list connection-list--${variant} ${className} h-full`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "bg-top-row-background md:h-full md:w-full shadow inline-block align-middle"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("table", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("tr", null, _ConnectionList_json__WEBPACK_IMPORTED_MODULE_2__.map((column, index) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
    key: index,
    className: 'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border'
  }, column.title))), formData.map((data, index) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("tr", {
    key: index
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
    id: 'connection_title--' + index,
    className: 'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border text-form-text'
  }, data.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
    id: 'connection_type--' + index,
    className: 'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border text-form-text'
  }, data.type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
    id: 'connection_comments--' + index,
    className: 'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border text-form-text'
  }, data.comments)))))));
};

ConnectionList.propTypes = {
  formData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
ConnectionList.defaultProps = {
  formData: [{
    "title": "1"
  }, {
    "type": "2"
  }, {
    "lastly": "3"
  }],
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (ConnectionList);

/***/ }),

/***/ "./components/ConnectionSection/ConnectionSection.jsx":
/*!************************************************************!*\
  !*** ./components/ConnectionSection/ConnectionSection.jsx ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _AddConnectionForm_AddConnectionForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../AddConnectionForm/AddConnectionForm */ "./components/AddConnectionForm/AddConnectionForm.jsx");
/**
 * @url ConnectionSection.js
 */

 // Import components



const ConnectionSection = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `connection-section connection-section--${variant} ${className} md:w-30 md:h-auto border-solid border-standard-border bg-container-background`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AddConnectionForm_AddConnectionForm__WEBPACK_IMPORTED_MODULE_2__["default"], null));
};

ConnectionSection.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
ConnectionSection.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (ConnectionSection);

/***/ }),

/***/ "./components/DeploymentName/DeploymentName.jsx":
/*!******************************************************!*\
  !*** ./components/DeploymentName/DeploymentName.jsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/**
 * @file DeploymentName.js
 */



const DeploymentName = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `deployment-name deployment-name--${variant} ${className} inline`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "headertext inline md:ml-4 md:p-2 text-green"
  }, "Customer Deployment Name"));
};

DeploymentName.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
DeploymentName.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (DeploymentName);

/***/ }),

/***/ "./components/HeaderPrimary/HeaderPrimary.jsx":
/*!****************************************************!*\
  !*** ./components/HeaderPrimary/HeaderPrimary.jsx ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Logo_Logo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Logo/Logo */ "./components/Logo/Logo.jsx");
/* harmony import */ var _DeploymentName_DeploymentName__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../DeploymentName/DeploymentName */ "./components/DeploymentName/DeploymentName.jsx");
/* harmony import */ var _QuickLinks_QuickLinks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../QuickLinks/QuickLinks */ "./components/QuickLinks/QuickLinks.jsx");
/**
* @file HeaderPrimary.js
*/






const HeaderPrimary = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `header-primary header-primary--${variant} ${className}`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "header inline-block bg-top-row-background shadow w-full align-middle md:pb-6",
    style: {
      height: 60 + 'px'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "md:pt-4 md:pl-5 flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-2/5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Logo_Logo__WEBPACK_IMPORTED_MODULE_2__["default"], {
    logoImageSrc: '../../../static/ntt_logo.png'
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DeploymentName_DeploymentName__WEBPACK_IMPORTED_MODULE_3__["default"], null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-1/5"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-1/5"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "user inline align-middle text-dark md:text-md w-1/5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_QuickLinks_QuickLinks__WEBPACK_IMPORTED_MODULE_4__["default"], {
    searchImage: '../../../static/search.png',
    notificationImage: '../../../static/notification.png',
    userImage: '../../../static/user.png'
  })))));
};

HeaderPrimary.propTypes = {
  headerBarImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  logoImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  searchImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  notificationImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  userImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
HeaderPrimary.defaultProps = {
  headerBarImage: '',
  logoImage: '',
  searchImage: '',
  notificationImage: '',
  userImage: '',
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (HeaderPrimary);

/***/ }),

/***/ "./components/HeaderSecondary/HeaderSecondary.jsx":
/*!********************************************************!*\
  !*** ./components/HeaderSecondary/HeaderSecondary.jsx ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _PanelTitle_PanelTitle__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../PanelTitle/PanelTitle */ "./components/PanelTitle/PanelTitle.jsx");
/**
 * @file HeaderSecondary.js
 */




const HeaderSecondary = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `header-secondary header-secondary--${variant} ${className}`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PanelTitle_PanelTitle__WEBPACK_IMPORTED_MODULE_2__["default"], {
    panelName: 'EDIT | CUSTOMER CORE'
  }));
};

HeaderSecondary.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
HeaderSecondary.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (HeaderSecondary);

/***/ }),

/***/ "./components/HeaderSection/HeaderSection.jsx":
/*!****************************************************!*\
  !*** ./components/HeaderSection/HeaderSection.jsx ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _HeaderPrimary_HeaderPrimary__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../HeaderPrimary/HeaderPrimary */ "./components/HeaderPrimary/HeaderPrimary.jsx");
/* harmony import */ var _HeaderSecondary_HeaderSecondary__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../HeaderSecondary/HeaderSecondary */ "./components/HeaderSecondary/HeaderSecondary.jsx");
/**
 * @file HeaderSection.js
 */

 // Import components




const HeaderSection = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `header-section header-section--${variant} ${className}`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HeaderPrimary_HeaderPrimary__WEBPACK_IMPORTED_MODULE_2__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HeaderSecondary_HeaderSecondary__WEBPACK_IMPORTED_MODULE_3__["default"], null));
};

HeaderSection.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
HeaderSection.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (HeaderSection);

/***/ }),

/***/ "./components/Layout.js":
/*!******************************!*\
  !*** ./components/Layout.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


/* Font Awesome */
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { fab } from '@fortawesome/free-brands-svg-icons'
// import { faCheckSquare, faCoffee, faEnvelope, faArrowDown, faPhoneAlt, faPhone } from '@fortawesome/free-solid-svg-icons'
// library.add(fab, faCheckSquare, faCoffee, faEnvelope, faArrowDown, faPhoneAlt, faPhone )

class Layout extends react__WEBPACK_IMPORTED_MODULE_1__["Component"] {
  render() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_0___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, `${this.props.pageMeta.title}`), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
      charSet: "UTF-8"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
      name: "viewport",
      content: "width=device-width, initial-scale=1.0"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
      name: "author",
      content: "John Bentley"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
      name: "description",
      content: this.props.pageMeta.description
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
      name: "keywords",
      content: this.props.pageMeta.keywords.join(',')
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
      rel: "shortcut icon",
      href: "/static/favicon.ico"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
      href: "https://fonts.googleapis.com/css?family=Catamaran&display=swap",
      rel: "stylesheet",
      async: true
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
      rel: "canonical",
      href: this.props.pageMeta.URL
    })));
  }

}

;
/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./components/Logo/Logo.jsx":
/*!**********************************!*\
  !*** ./components/Logo/Logo.jsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/**
* @file Logo.js
*/



const Logo = props => {
  const {
    logoImageSrc,
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: `logo logo--${variant} ${className} align-middle inline w-20 inline`,
    src: logoImageSrc,
    alt: ""
  });
};

Logo.propTypes = {
  logoImageSrc: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
Logo.defaultProps = {
  logoImageSrc: '../../../static/ntt_logo.png',
  tagName: 'img',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (Logo);

/***/ }),

/***/ "./components/MainPanelSection/MainPanelSection.jsx":
/*!**********************************************************!*\
  !*** ./components/MainPanelSection/MainPanelSection.jsx ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ConnectionSection_ConnectionSection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ConnectionSection/ConnectionSection */ "./components/ConnectionSection/ConnectionSection.jsx");
/**
* @file MainPanelSection.js
*/

 // Import Components



const MainPanelSection = props => {
  const {
    panelName,
    panelActivityName,
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `main-panel-section main-panel-section--${variant} ${className} w-100`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "text-xl pb-10"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    className: "font-bold"
  }, panelName), " | ", panelActivityName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ConnectionSection_ConnectionSection__WEBPACK_IMPORTED_MODULE_2__["default"], null));
};

MainPanelSection.propTypes = {
  panelName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  panelActivity: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
MainPanelSection.defaultProps = {
  panelName: 'Headings',
  panelActivityName: 'Add Heading',
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (MainPanelSection);

/***/ }),

/***/ "./components/NavigationListing/NavigationListing.json":
/*!*************************************************************!*\
  !*** ./components/NavigationListing/NavigationListing.json ***!
  \*************************************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"title\":\"Common Data Objects\",\"image\":\"../../../static/CDO.png\",\"Sublinks\":[\"person_master\",\"Violation History\",\"address_lookup\",\"vehicle_master\",\"violation_lookup\",\"Telemetry\",\"Sample\",\"New CDO\",\"Add CDO\"]},{\"title\":\"Connections\",\"image\":\"../../../static/EPU.png\",\"Sublinks\":[\"Senstar V7\",\"epu_object_counts_3\",\"Sample\",\"Add EPU Package\"]},{\"title\":\"Sensors\",\"image\":\"../../../static/Connections.png\",\"Sublinks\":[\"Sample\",\"Sample2\"]}]");

/***/ }),

/***/ "./components/NavigationListing/NavigationListing.jsx":
/*!************************************************************!*\
  !*** ./components/NavigationListing/NavigationListing.jsx ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _NavigationListing_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./NavigationListing.json */ "./components/NavigationListing/NavigationListing.json");
var _NavigationListing_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./NavigationListing.json */ "./components/NavigationListing/NavigationListing.json", 1);
/**
 * @file NavigationListing.js
 */




const NavigationListing = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return (
    /*#__PURE__*/
    // Display the Navigation Menu items that came from JSON
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: `navigation-listing navigation-listing--${variant} ${className} nav`
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", null, _NavigationListing_json__WEBPACK_IMPORTED_MODULE_2__.map((listing, index) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "p-5 text-white align-middle shadow text-md",
      key: index
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "inline w-1/6 pr-2 h-auto",
      src: listing.image
    }), listing.title))))
  );
};

NavigationListing.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
NavigationListing.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (NavigationListing);

/***/ }),

/***/ "./components/PanelTitle/PanelTitle.jsx":
/*!**********************************************!*\
  !*** ./components/PanelTitle/PanelTitle.jsx ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/** 
* @file PanelTitle.js
*/



const PanelTitle = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
    panelName
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `panel-title panel-title--${variant} ${className} bg-edit-background text-white flex w-full h-auto md:mb-10 md:py-6 md:pl-6 shadow align-middle`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-white font-sm m-0 w-4/5 md:ml-3"
  }, " ", panelName, " "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "align-middle w-1/5 md:pr-4 text-right"
  }, "X"));
};

PanelTitle.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,
  panelName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string
};
PanelTitle.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
  panelName: 'Default Panel Name'
};
/* harmony default export */ __webpack_exports__["default"] = (PanelTitle);

/***/ }),

/***/ "./components/QuickLinks/QuickLinks.jsx":
/*!**********************************************!*\
  !*** ./components/QuickLinks/QuickLinks.jsx ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/**
 * 
 * @file QuickLinks.js 
 */



const QuickLinks = props => {
  const {
    searchImage,
    notificationImage,
    userImage,
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `quick-links quick-links--${variant} ${className} inline-block`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "md:pl-6 md:w-1/4 h-auto inline",
    src: searchImage,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "md:pl-6 md:w-1/4 h-auto inline",
    src: notificationImage,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: "md:pl-6 md:w-1/4 h-auto inline",
    src: userImage,
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "N. Joy"));
};

QuickLinks.propTypes = {
  searchImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  notificationImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  userImage: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
QuickLinks.defaultProps = {
  searchImage: '',
  notificationImage: '',
  userImage: '',
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (QuickLinks);

/***/ }),

/***/ "./components/SidebarSection/SidebarSection.jsx":
/*!******************************************************!*\
  !*** ./components/SidebarSection/SidebarSection.jsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _NavigationListing_NavigationListing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../NavigationListing/NavigationListing */ "./components/NavigationListing/NavigationListing.jsx");
/**
 * @file SidebarSection.js
 */

 // Import components



const SidebarSection = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: `sidebar-section sidebar-section--${variant} ${className} flex-initial w-1/5`
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_NavigationListing_NavigationListing__WEBPACK_IMPORTED_MODULE_2__["default"], {
    style: ''
  }));
};

SidebarSection.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
SidebarSection.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (SidebarSection);

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _components_HeaderSection_HeaderSection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/HeaderSection/HeaderSection */ "./components/HeaderSection/HeaderSection.jsx");
/* harmony import */ var _components_SidebarSection_SidebarSection__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/SidebarSection/SidebarSection */ "./components/SidebarSection/SidebarSection.jsx");
/* harmony import */ var _components_MainPanelSection_MainPanelSection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/MainPanelSection/MainPanelSection */ "./components/MainPanelSection/MainPanelSection.jsx");
/**
 * @file index.js
 */
// Import dependencies

 // Import components






const Home = props => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "home"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
    pageMeta: {
      title: "DACI in NextJS",
      keywords: ["DACI"],
      description: "This is DACI. Built using Flask and NextJS",
      URL: "https://www.nttdata.com"
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_HeaderSection_HeaderSection__WEBPACK_IMPORTED_MODULE_3__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_SidebarSection_SidebarSection__WEBPACK_IMPORTED_MODULE_4__["default"], null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "flex-initial w-4/5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_MainPanelSection_MainPanelSection__WEBPACK_IMPORTED_MODULE_5__["default"], {
    panelName: 'Connections',
    panelActivityName: 'Add Connection'
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("style", {
    global: true,
    jsx: true
  }, `
        :root {
          --sidebar-margin-left: 20px;
          --sidebar-width: 285px;
          --menu-item-height: 50px;
          --column-width:200px;
          --button-offset:30px;
          --checkbox-offset:230px;
          --cdotable-width:800px;
        }

        html, body {
          height: 100%;
          width: 100%;
          margin: 0;
          padding: 0;
          background-color: #2C2D31;
          font-family: Catamaran, sans-serif;
          color: white;
        }

        select {
          display: inline-block;
          box-sizing: border-box;
          height: 30px;
          border: 1px solid #53555A;
          border-radius: 15.5px;
          background-color: #34373F;
          padding-left: 15px;
          font-family: Catamaran;
          color: #6C727E;
          font-size: 14px;
          line-height: 22px;
        }
        
        select:focus-within {
          border: 1px solid #72BF44;
          outline: none;
        }

        .nav {
          width: var(--sidebar-width);
          vertical-align: middle;
          height: 100%;
          background-color: #35373F;
          position: absolute;
          display: inline-block;
          overflow: auto;
          box-shadow: inset 0 1px 3px 0 rgba(0,0,0,0.5), 0 3px 6px 0 rgba(0,0,0,0.5);
          margin-left: var(--sidebar-margin-left);
          -ms-overflow-style: none;
          scrollbar-width: none;
          -ms-overflow-style: none;  /* Internet Explorer 10+ */
        }

        .nav::-webkit-scrollbar { /* WebKit */
          width: 0;
          height: 0;
        }
        
        .nav .arrow {
          height: 16px;
          width: 16px;
          margin-right: 5px;
        }

        .nav .link {
          position: absolute;
          top: 10px;
          left: 15px;
          height: 20px;
          width: 20px;
        }

        .template {
          display: inline-block;
          margin-left: 350px;
          position: relative;
        }
        
        #page-wrapper {
          position: absolute;
          top: 0;
        }

        #title {
          font-size: 22px;
          margin-top: 10px;
        }

      `));
};

Home.propTypes = {
  tagName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  variant: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf(['default']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node
};
Home.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
};
/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/johnbentley/Desktop/Projects/daci-react-flask/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-jsonschema-form":
/*!****************************************!*\
  !*** external "react-jsonschema-form" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-jsonschema-form");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map