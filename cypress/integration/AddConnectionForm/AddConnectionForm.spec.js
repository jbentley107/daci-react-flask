describe('Add New Connection', () => {  
  it('Opens the DACI application, adds two new connections and submits them', () => {
    cy.viewport(1900, 800);
    // Need to visit the localhost:3000 (DACI app) page
    cy.visit('https://daci-react-flask.now.sh/');

    for (var i = 0; i < 5; i++) {
      // Click inside the 'root_title' ID 
      cy.get('#root_title')
      .type('Test ' + i) // Type in 'Test1'
      .should('have.value', 'Test ' + i) // Check the value of the field

      // Type in the Comments field, make sure it has been typed into
      cy.get('#root_comments')
        .type('Test ' + i)
        .should('have.value', 'Test ' + i)

      // Use the type select element to choose an option, verify it is filled in
      cy.get('#root_type')
        .select('PostgreSQL')
        .should('have.value', 'PostgreSQL')

      // Submit the new connection
      cy.get('#add_connection_submit')
        .click()

      // Verify each of the fields in the <ConnectionList /> have the correct values
      cy.get('#connection_title--' + i)
        .should('contain', 'Test ' + i) 

      cy.get('#connection_type--' + i)
        .should('contain', 'PostgreSQL')

      cy.get('#connection_comments--' + i)
        .should('contain', 'Test ' + i)
      }
  })
})