/**
 * @file _app.js
 */ 
import React from 'react';

// Import styling 
import '../styles/styles.css';

// Import components
import Home from './index';

const App = () => {

  return (
    <div>
      <Home />
    </div>
  )
}

export default App;