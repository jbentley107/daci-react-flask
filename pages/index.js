/**
 * @file index.js
 */ 
// Import dependencies
import React from 'react';
import PropTypes from 'prop-types';

// Import components
import Layout from '../components/Layout';
import HeaderSection from '../components/HeaderSection/HeaderSection';
import SidebarSection from '../components/SidebarSection/SidebarSection';
import MainPanelSection from '../components/MainPanelSection/MainPanelSection';

const Home = (props) => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
  } = props;

  return (
    <div className="home">
      <Layout 
        pageMeta={{
          title: "DACI in NextJS",
          keywords: ["DACI"],
          description: "This is DACI. Built using Flask and NextJS",
          URL: "https://www.nttdata.com"
        }}
      />
      <HeaderSection/>

      <div className="flex">
        <SidebarSection/>
        <div className="flex-initial w-4/5">
          <MainPanelSection 
            panelName={'Connections'}
            panelActivityName={'Add Connection'}
          />
        </div>
      </div>

      <style global jsx>{`
        :root {
          --sidebar-margin-left: 20px;
          --sidebar-width: 285px;
          --menu-item-height: 50px;
          --column-width:200px;
          --button-offset:30px;
          --checkbox-offset:230px;
          --cdotable-width:800px;
        }

        html, body {
          height: 100%;
          width: 100%;
          margin: 0;
          padding: 0;
          background-color: #2C2D31;
          font-family: Catamaran, sans-serif;
          color: white;
        }

        select {
          display: inline-block;
          box-sizing: border-box;
          height: 30px;
          border: 1px solid #53555A;
          border-radius: 15.5px;
          background-color: #34373F;
          padding-left: 15px;
          font-family: Catamaran;
          color: #6C727E;
          font-size: 14px;
          line-height: 22px;
        }
        
        select:focus-within {
          border: 1px solid #72BF44;
          outline: none;
        }

        .nav {
          width: var(--sidebar-width);
          vertical-align: middle;
          height: 100%;
          background-color: #35373F;
          position: absolute;
          display: inline-block;
          overflow: auto;
          box-shadow: inset 0 1px 3px 0 rgba(0,0,0,0.5), 0 3px 6px 0 rgba(0,0,0,0.5);
          margin-left: var(--sidebar-margin-left);
          -ms-overflow-style: none;
          scrollbar-width: none;
          -ms-overflow-style: none;  /* Internet Explorer 10+ */
        }

        .nav::-webkit-scrollbar { /* WebKit */
          width: 0;
          height: 0;
        }
        
        .nav .arrow {
          height: 16px;
          width: 16px;
          margin-right: 5px;
        }

        .nav .link {
          position: absolute;
          top: 10px;
          left: 15px;
          height: 20px;
          width: 20px;
        }

        .template {
          display: inline-block;
          margin-left: 350px;
          position: relative;
        }
        
        #page-wrapper {
          position: absolute;
          top: 0;
        }

        #title {
          font-size: 22px;
          margin-top: 10px;
        }

      `}</style>
    </div>
  )
}

Home.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
}

Home.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default Home
