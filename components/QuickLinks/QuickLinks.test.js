/**
 * @file QuickLinks.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import QuickLinks from './QuickLinks.jsx'

describe('components', () => {
  describe('QuickLinks', () => {
    it('should mount with props', function () {
      const wrap = mount(<QuickLinks>Hello World</QuickLinks>)

      const expectedProps = {
        searchImage: '',
        notificationImage: '',
        userImage: '',
        tagName: 'div',
        className: '',
        variant: 'default',
        children: 'Hello World'
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<QuickLinks>Hello World</QuickLinks>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('div')
      // contain styles
      expect(wrap[0].attribs.class).toContain('quick-links')

      // contain text
      expect(wrap[0].children[0].type).toBe('tag')
    })
  })
})