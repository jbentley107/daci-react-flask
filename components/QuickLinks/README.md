# QuickLinks

## Props

Property | Values | Default | Description
--- | --- | --- | ---
`searchImage` | `[String]` Any | `'img'` | Defines the path to the search image.
`notificationImage` | `[String]` Any | `'img'` | Defines the path to the notification image.
`userImage` | `[String]` Any | `'img'` | Defines the path to the user image.
`tagName` | `[String]` Any | `'div'` | Defines the HTML tagName output by the tag.
`className` | `[String]` Any | `''` | Defines arbitrary className to add to the component's class list.
`variant` | `[String]` Any | `'default'` | Defines BEM modifier to add to class list, e.g., `'{{className}}--default'`.
`children` | `[Object]` | `undefined` | Defines the children elements passed to the component.
