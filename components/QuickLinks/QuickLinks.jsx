/**
 * 
 * @file QuickLinks.js 
 */
import React from 'react';
import PropTypes from 'prop-types';

const QuickLinks = (props) => {
    const {
        searchImage, 
        notificationImage, 
        userImage,
        tagName: Tag,
        className,
        variant,
        children
    } = props;

    return (
        <div className={`quick-links quick-links--${variant} ${className} inline-block`}>
            <img className="md:pl-6 md:w-1/4 h-auto inline" src={searchImage} alt="" />
            <img className="md:pl-6 md:w-1/4 h-auto inline" src={notificationImage} alt="" />
            <img className="md:pl-6 md:w-1/4 h-auto inline" src={userImage} alt="" /> 
            <span>N. Joy</span>
        </div>
    )
}

QuickLinks.propTypes = {
    searchImage: PropTypes.string,
    notificationImage: PropTypes.string,
    userImage: PropTypes.string,
    tagName: PropTypes.string,
    className: PropTypes.string,
    variant: PropTypes.oneOf(['default']),
    children: PropTypes.node
  };
    
  QuickLinks.defaultProps = {
    searchImage: '',
    notificationImage: '',
    userImage: '',
    tagName: 'div',
    className: '',
    variant: 'default',
    children: ''
  }

export default QuickLinks;