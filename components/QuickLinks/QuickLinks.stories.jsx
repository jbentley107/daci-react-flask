/**
 * QuickLinks.js
 */
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import searchImage from '../../static/search.png';
import notificationImage from '../../static/notification.png';
import userImage from '../../static/user.png';

// Mocks/Utils
import { StorybookRouter } from '../../mocks/next/router'

// Component(s)
import QuickLinks from './QuickLinks';

// Decorators
const CenterDecorator = storyFn => (
  <div>
    { storyFn() }
  </div>
);

const RouterDecorator = (storyFn) => (
  <StorybookRouter>{storyFn()}</StorybookRouter>
)
storiesOf('QuickLinks', module)
  .addDecorator(RouterDecorator)
  .addDecorator(CenterDecorator)
  .add('with required props', () => (
    <QuickLinks 
        notificationImage={searchImage}
        notificationImage={notificationImage}
        userImage={userImage}
    />
  ));