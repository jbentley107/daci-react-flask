# ConnectionButton

## Props

Property | Values | Default | Description
--- | --- | --- | ---
`form` | `[String]` Any | `'button'` | Sets the 'form' attribute for the <button> element.
`type` | `[String]` Any | `'button'` | Sets the 'type' attribute for the <button> element.
`text` | `[String]` Any | `'button'` | Defines the text that appears inside of the button.
`hierarchy` | `[String]` Any | `'button'` | Defines the button hierarchy. Enter a string like 'secondary' or 'primary'.
`tagName` | `[String]` Any | `'div'` | Defines the HTML tagName output by the tag.
`className` | `[String]` Any | `''` | Defines arbitrary className to add to the component's class list.
`id` | `[String]` Any | `''` | Defines arbitrary id to add to the component.
`variant` | `[String]` Any | `'default'` | Defines BEM modifier to add to class list, e.g., `'{{className}}--default'`.
`children` | `[Object]` | `undefined` | Defines the children elements passed to the component.
