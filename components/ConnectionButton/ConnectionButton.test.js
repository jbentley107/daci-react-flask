/**
 * @file ConnectionButton.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import ConnectionButton from './ConnectionButton.jsx'

describe('components', () => {
  describe('ConnectionButton', () => {
    it('should mount with props', function () {
      const wrap = mount(<ConnectionButton>Hello World</ConnectionButton>)

      const expectedProps = {
        form: 'form',
        type: 'button',
        text: 'Button',
        tagName: 'div',
        className: '',
        variant: 'default',
        children: 'Hello World',
        id: '',
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<ConnectionButton>Hello World</ConnectionButton>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('button')
      // contain styles
      expect(wrap[0].attribs.class).toContain('connection-button')

      // contain text
      expect(wrap[0].children[0].type).toBe('text')
    })
  })
})