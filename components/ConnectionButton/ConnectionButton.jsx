/**
 * @file ConnectionButton.js
 */

// Import dependencies
import PropTypes from 'prop-types';
import React from 'react';

let additionalClasses = "";

// Function here to determine what type of button needs to be loaded
function determineAdditionalClasses (hierarchy) {
  // If a primary button is chosen, add the classes that are unique to that button
  if (hierarchy == 'primary') {
    additionalClasses = "ml-3 py-1 md:w-20 md:h-auto bg-primary-button-background text-white rounded-full border-primary-button-background text-center duration-500 font-display hover:cursor-pointer hover:shadow"
  } else {
    additionalClasses = "border-secondary-button-background text-secondary-button-background text-white hover:secondary-button-background-hover"
  }
}

const ConnectionButton = (props) => {
  const {
    disabled,
    form,
    type,
    text,
    hierarchy,
    tagName: Tag,
    className,
    id,
    variant,
    children
  } = props

  // Determine what classes to add to a button
  determineAdditionalClasses (hierarchy)

  if (!disabled) {
    return (
      <button form={form} id={id} type={type} className={`connection-button connection-button--${variant} ${className} ml-3 py-1 md:w-20 md:h-auto rounded-full border border-solid text-center duration-500 font-display hover:cursor-pointer` + ` ` + additionalClasses}>
        {text}
      </button>
    )
  } else {
    // Render a button in disabled state
    return (
      <button form={form} id={id} type={type} className={`connection-button connection-button--${variant} ${className} ml-3 py-1 md:w-20 md:h-auto rounded-full border border-solid text-center duration-500 font-display hover:cursor-pointer opacity-25 cursor-default` + ` ` + additionalClasses}>
        {text}
      </button>
    )
  }
}

ConnectionButton.propTypes = {
  form: PropTypes.string,
  type: PropTypes.string,
  text: PropTypes.string,
  hierarchy: PropTypes.string,
  tagName: PropTypes.string,
  className: PropTypes.string,
  id: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
}

ConnectionButton.defaultProps = {
  form: 'form',
  type: 'button',
  text: 'Button',
  tagName: 'div',
  className: '',
  id: '',
  variant: 'default',
  children: ''
}

export default ConnectionButton