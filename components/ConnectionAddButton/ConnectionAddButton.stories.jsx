/**
 * ConnectionAddButton.js
 */
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

// Mocks/Utils
import { StorybookRouter } from '../../mocks/next/router';

// Component(s)
import ConnectionAddButton from './ConnectionAddButton.jsx';

// Decorators
const CenterDecorator = storyFn => (
  <div>
    { storyFn() }
  </div>
);

const RouterDecorator = (storyFn) => (
  <StorybookRouter>{storyFn()}</StorybookRouter>
)
storiesOf('ConnectionAddButton', module)
  .addDecorator(RouterDecorator)
  .addDecorator(CenterDecorator)
  .add('with required props', () => (
    <ConnectionAddButton />
  ));