/**
 * @file ConnectionAddButton.js
 */

import React from 'react'
import PropTypes from 'prop-types'

const ConnectionAddButton = (props) => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
    buttonText
  } = props


  return (
    <div className={`connection-add-button connection-add-button--${variant} ${className} md:h-16 inline align-middle`}>
      <img className="align-middle inline md:p-3 md:w-16 md:h-auto" id="connections-add-row-icon" src="../../../static/add_in_circle.png" alt="" />
      <span id="add-connection-text" className="align-middle leading-none my-2 inline align-middle">{buttonText}</span>
    </div>
  )
}

ConnectionAddButton.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
	buttonText: PropTypes.string
}

ConnectionAddButton.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
  buttonText: 'Default Text'
}

export default ConnectionAddButton;