/**
 * @file ConnectionAddButton.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import ConnectionAddButton from './ConnectionAddButton.jsx'

describe('components', () => {
  describe('ConnectionAddButton', () => {
    it('should mount with props', function () {
      const wrap = mount(<ConnectionAddButton>Hello World</ConnectionAddButton>)

      const expectedProps = {
        children: 'Hello World',
        tagName: 'div',
        className: '',
        variant: 'default',
        buttonText: 'Default Text'
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<ConnectionAddButton>Hello World</ConnectionAddButton>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('div')
      // contain styles
      expect(wrap[0].attribs.class).toContain('connection-add-button')

      // contain text
      expect(wrap[0].children[0].type).toBe('tag')
    })
  })
})