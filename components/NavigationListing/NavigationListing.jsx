/**
 * @file NavigationListing.js
 */
import React from "react";
import PropTypes from 'prop-types';

import listings from './NavigationListing.json'

const NavigationListing = (props) => {
  const { 
    tagName: Tag,
    className,
    variant,
    children
  } = props;

    return (
      // Display the Navigation Menu items that came from JSON
      <div className={`navigation-listing navigation-listing--${variant} ${className} nav`}>
        <ul>                     
          {listings.map((listing, index) => (
            <li className="p-5 text-white align-middle shadow text-md" key={ index }>
              <img className="inline w-1/6 pr-2 h-auto" src={ listing.image } />
              { listing.title }
            </li>
          ))}
        </ul>
      </div>
    );
};

NavigationListing.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
};

NavigationListing.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default NavigationListing;