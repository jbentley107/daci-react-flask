/**
 * @file NavigationListing.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import NavigationListing from './NavigationListing.jsx'

describe('components', () => {
  describe('NavigationListing', () => {
    it('should mount with props', function () {
      const wrap = mount(<NavigationListing>Hello World</NavigationListing>)

      const expectedProps = {
        children: 'Hello World',
        tagName: 'div',
        className: '',
        variant: 'default'
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<NavigationListing>Hello World</NavigationListing>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('div')
      // contain styles
      expect(wrap[0].attribs.class).toContain('navigation-listing')

      // contain text
      expect(wrap[0].children[0].type).toBe('tag')
    })
  })
})