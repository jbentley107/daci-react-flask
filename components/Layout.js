import Head from 'next/head'
import React, { Component } from 'react'

/* Font Awesome */
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { fab } from '@fortawesome/free-brands-svg-icons'
// import { faCheckSquare, faCoffee, faEnvelope, faArrowDown, faPhoneAlt, faPhone } from '@fortawesome/free-solid-svg-icons'

// library.add(fab, faCheckSquare, faCoffee, faEnvelope, faArrowDown, faPhoneAlt, faPhone )

class Layout extends Component {

  render() {
    return (
      <>
        <Head>
          <title>{`${this.props.pageMeta.title}`}</title>

          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta name="author" content="John Bentley" />

          <meta name="description" content={this.props.pageMeta.description} />

          <meta name="keywords" content={this.props.pageMeta.keywords.join(',')} />

          <link rel="shortcut icon" href="/static/favicon.ico" />

          <link href="https://fonts.googleapis.com/css?family=Catamaran&display=swap" rel="stylesheet" async />

          <link rel="canonical" href={this.props.pageMeta.URL} />
        </Head>
      </>
    )
  }
};

export default Layout;