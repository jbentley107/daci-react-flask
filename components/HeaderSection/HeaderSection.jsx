/**
 * @file HeaderSection.js
 */
import React from 'react';
import PropTypes from 'prop-types';

// Import components
import HeaderPrimary from '../HeaderPrimary/HeaderPrimary';
import HeaderSecondary from '../HeaderSecondary/HeaderSecondary';

const HeaderSection = (props) => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props;
  
  return (
      <div className={`header-section header-section--${variant} ${className}`}>
        <HeaderPrimary/>
        <HeaderSecondary />
      </div>
  )
}

HeaderSection.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
};
  
HeaderSection.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default HeaderSection;