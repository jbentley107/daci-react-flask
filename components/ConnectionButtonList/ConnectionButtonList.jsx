/**
 * @file ConnectionButtonList.js
 */

import React from 'react';
import PropTypes from 'prop-types';

// Import components
import ConnectionButton from '../ConnectionButton/ConnectionButton'

const ConnectionButtonList = (props) => {
  const { 
    tagName: Tag,
    className,
    variant,
    children
  } = props;

  return (
    <div className={`connection-button-list connection-button-list--${variant} ${className} inline text-center align-middle md:h-16 md:mr-3`}>
      <ConnectionButton 
        text="Test"
        type="button"
        hierarchy="secondary"
      />
      <ConnectionButton 
        form="add-connection-form"
        text="Save"
        type="submit"
        hierarchy="primary"
        id={'add_connection_submit'}
      />
    </div>
  )
}

ConnectionButtonList.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
};
  
ConnectionButtonList.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default ConnectionButtonList