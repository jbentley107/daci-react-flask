/**
 * @file ConnectionDataPoints
 */

import React from 'react';
import PropTypes from 'prop-types';

const ConnectionDataPoints = (props) => {
  const { 
    tagName: Tag,
    className,
    variant,
    children
  } = props;

  return (
    <div id="connections" className={`connection-data-points connection-data-points--${variant} ${className}`} >
      <div id="MariaDB">
        <div className="md:mt-2 md:ml-4 inline-block">
          <p>Host</p>
          {/* {{ form.host_name }} {{ form.oldhost_name }} */}
        </div>
        <div className="md:mt-2 md:ml-4 inline-block">
          <p>Port</p>
          {/* {{ form.port_number }} {{ form.oldport_number }} */}
        </div>
        <div className="md:mt-2 md:ml-4 inline-block">
          <p>User name</p>
          {/* {{ form.user_name }} {{ form.olduser_name }} */}
        </div>
        <div className="md:mt-2 md:ml-4 inline-block">
          <p>Password</p>
          {/* {{ form.password }} {{ form.oldpassword }} */}
        </div>
        <div className="md:mt-2 md:ml-4 inline-block">
          <p>Database</p>
          {/* {{ form.database }} {{ form.olddatabase }} */}
        </div>
        <div className="md:mt-2 md:ml-4 inline-block">
          <p>URL</p>
          {/* {{ form.URL }} {{ form.oldURL }} */}
        </div>
      </div>
      <div id="MongoDB"></div>
      <div id="OtherDB"></div>
    </div>
  )
}

ConnectionDataPoints.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
};
  
ConnectionDataPoints.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default ConnectionDataPoints