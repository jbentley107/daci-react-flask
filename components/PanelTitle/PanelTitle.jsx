/** 
* @file PanelTitle.js
*/ 
import React from 'react';
import PropTypes from 'prop-types';

const PanelTitle = (props) => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
    panelName
  } = props;
  
  return (
    <div className={`panel-title panel-title--${variant} ${className} bg-edit-background text-white flex w-full h-auto md:mb-10 md:py-6 md:pl-6 shadow align-middle`}>
      <p className="text-white font-sm m-0 w-4/5 md:ml-3"> {panelName} </p>
      <span className="align-middle w-1/5 md:pr-4 text-right">X</span>
    </div>
  )
}

PanelTitle.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
  panelName: PropTypes.string
};
  
PanelTitle.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
  panelName: 'Default Panel Name'
}

export default PanelTitle;