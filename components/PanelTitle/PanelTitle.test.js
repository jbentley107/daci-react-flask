/**
 * @file PanelTitle.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import PanelTitle from './PanelTitle.jsx'

describe('components', () => {
  describe('PanelTitle', () => {
    it('should mount with props', function () {
      const wrap = mount(<PanelTitle>Hello World</PanelTitle>)

      const expectedProps = {
        tagName: 'div',
        className: '',
        variant: 'default',
        children: 'Hello World',
        panelName: 'Default Panel Name'
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<PanelTitle>Hello World</PanelTitle>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('div')
      // contain styles
      expect(wrap[0].attribs.class).toContain('panel-title')

      // contain text
      expect(wrap[0].children[0].type).toBe('tag')
    })
  })
})