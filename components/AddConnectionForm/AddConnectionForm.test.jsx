/**
 * @file AddConnectionForm.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import AddConnectionForm from './AddConnectionForm.jss'

describe('components', () => {
  describe('AddConnectionForm', () => {
    it('should mount with props', function () {
      const wrap = mount(<AddConnectionForm>Hello World</AddConnectionForm>)

      const expectedProps = {
        children: 'Hello World',
        tagName: 'div',
        className: '',
        variant: 'default'
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<AddConnectionForm>Hello World</AddConnectionForm>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('div')
      // contain styles
      expect(wrap[0].attribs.class).toContain('add-connection-form')

      // contain text
      expect(wrap[0].children[0].type).toBe('tag')
    })

    it('should display all labels of the form', function () {
      const wrap = render(<AddConnectionForm />)

      expect(wrap[0].children[2].children[1].toEqual('form'))
    })
  })
})