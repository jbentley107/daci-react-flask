/**
 * @file AddConnectionForm.js
 */
// Import dependencies
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Form from 'react-jsonschema-form';

import ConnectionButton from '../ConnectionButton/ConnectionButton';
import ConnectionAddButton from '../ConnectionAddButton/ConnectionAddButton';
import ConnectionList from '../ConnectionList/ConnectionList';

import schema from './AddConnectionForm.json'

const AddConnectionForm = (props) => {
  const {
    tagName: Tag,
    className,
    variant,
    children
  } = props

  // Set up our variables that will control various states
  const [showConnectionList, setShowConnectionList] = useState(false) // State to show/hide connections
  const [isTestButtonDisabled, setIsTestButtonDisabled] = useState(true)
  const [isSaveButtonDisabled, setIsSaveButtonDisabled] = useState(true)
  const [formDataState, setFormDataState] = useState({});

  // Use this object to add special attributes to form fields
  // Examples: classNames, id, or special form element types
  const uiSchema = {
    comments: {
      "ui:widget": "textarea",
      classNames: "form-control",
      value: " "
    }
  }

  let currentFormData = {};

  let [
    // The array we will be passing into the <ConnectionList />
    returnedFormData = [], 
    // The function that actually updates the <ConnectionList /> state
    setFormData
  ] = useState([]); // the initial state of the <ConnectionList />

  function onChange ({formData}, e) {
    // Store the live form data in a variable (pass by value)
    currentFormData = JSON.parse(JSON.stringify(formData));

    // Check if a value has been entered into the critical fields
    if (formData.title && formData.comments && formData.type && isTestButtonDisabled) {
      setIsTestButtonDisabled(false); // React re-renders the form here
      setIsSaveButtonDisabled(false);
      // Update the form data state with the values pre-render
      setFormDataState(JSON.parse(JSON.stringify(currentFormData)));
    }    
  }

  // This function runs when the Form is submitted
  // It returns some form data, and sets the state for the ConnectionList
  function onSubmit ({formData}, e) {
    // We must create a copy of the array because of JS pass by reference
    let formDataArray = returnedFormData.slice();
    // Add the latest form data to the array of form data objects
    formDataArray.push(formData);

    // Pass the updated array of form data objects to the 
    // ComponentList's state
    setFormData(formDataArray);
    setShowConnectionList(true);
    // Reset the field states and the buttons
    setIsTestButtonDisabled(true);
    setIsSaveButtonDisabled(true);
    setFormDataState({});
  }

  return (
    <div id="defaultConnection" className={`add-connection-form add-connection-form`}>
      {/* Displays the form data only if some data exists
      (the user has submitted the form) */}
      { showConnectionList ? <ConnectionList formData={returnedFormData} /> : null }
      <div className={`connection-header connection-header bg-top-row-background md:h-16 md:w-full shadow flex align-middle`}>
        <ConnectionAddButton 
          buttonText={"Add Connection"}
          className={'w-3/5'}
        />

        <div className={`connection-button-list inline text-center align-middle md:h-16 md:mr-3 w-2/5 m-3`}>
          <ConnectionButton 
            disabled={isTestButtonDisabled}
            form="add-connection-form"
            text="Test"
            type="button"
            hierarchy="secondary"
          />
          <ConnectionButton 
            disabled={isSaveButtonDisabled}
            form="add-connection-form"
            text="Save"
            type="submit"
            hierarchy="primary"
            id={'add_connection_submit'}
          />
        </div>
      </div>
      <div className="inline-block border-solid h-auto md:mt-6 text-sm w-full">
        <div className="inline md:ml-4 md:mt-2 text-form-text">
            <Form
              id="add-connection-form"
              className="react-jsonschema-form pl-10 w-3/5"
              schema={schema}
              uiSchema={uiSchema}
              onChange={onChange}
              onSubmit={onSubmit}
              formData={formDataState}
            />
        </div>
      </div>
    </div>
  )
}

AddConnectionForm.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

AddConnectionForm.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default AddConnectionForm;