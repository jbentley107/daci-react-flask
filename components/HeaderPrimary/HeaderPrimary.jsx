/**
* @file HeaderPrimary.js
*/ 
import React from 'react';
import PropTypes from 'prop-types';

import Logo from '../Logo/Logo';
import DeploymentName from '../DeploymentName/DeploymentName';
import QuickLinks from '../QuickLinks/QuickLinks';

const HeaderPrimary = (props) => {
    const {
        tagName: Tag,
        className,
        variant,
        children
      } = props;

    return (
        <div className={`header-primary header-primary--${variant} ${className}`}>
            <div className="header inline-block bg-top-row-background shadow w-full align-middle md:pb-6" style={{height: 60 + 'px'}}>
                <div className="md:pt-4 md:pl-5 flex">
                    <div className="w-2/5">
                        <Logo 
                            logoImageSrc={'../../../static/ntt_logo.png'}
                        />
                        <DeploymentName />
                    </div>
                    <div className="w-1/5" />
                    <div className="w-1/5" />
                    <div className="user inline align-middle text-dark md:text-md w-1/5">
                        <QuickLinks 
                            searchImage={'../../../static/search.png'}
                            notificationImage={'../../../static/notification.png'}
                            userImage={'../../../static/user.png'}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

HeaderPrimary.propTypes = {
	headerBarImage: PropTypes.string,
	logoImage: PropTypes.string,
    searchImage: PropTypes.string,
    notificationImage: PropTypes.string,
    userImage: PropTypes.string,
    tagName: PropTypes.string,
    className: PropTypes.string,
    variant: PropTypes.oneOf(['default']),
    children: PropTypes.node
}

HeaderPrimary.defaultProps = {
    headerBarImage: '',
	logoImage: '',
    searchImage: '',
    notificationImage: '',
    userImage: '',
    tagName: 'div',
    className: '',
    variant: 'default',
    children: ''
}

export default HeaderPrimary