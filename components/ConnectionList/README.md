# ConnectionList

## Props

Property | Values | Default | Description
--- | --- | --- | ---
`formData` | `[Object]` Any | `'undefined'` | Defines the form data that is passed back from the JSON form submission.
`tagName` | `[String]` Any | `'div'` | Defines the HTML tagName output by the tag.
`className` | `[String]` Any | `''` | Defines arbitrary className to add to the component's class list.
`variant` | `[String]` Any | `'default'` | Defines BEM modifier to add to class list, e.g., `'connection-list--default'`.
`children` | `[Object]` | `undefined` | Defines the children elements passed to the component.
