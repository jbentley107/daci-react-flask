/**
 * @file ConnectionList.js
 */
import * as React from 'react';
import PropTypes from 'prop-types';

import columns from './ConnectionList.json';

const ConnectionList = (props) => {
  const {
    formData,
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  return (
    <Tag className={`connection-list connection-list--${variant} ${className} h-full`}>
      <div className="bg-top-row-background md:h-full md:w-full shadow inline-block align-middle">
        <table>
          <tbody>
            <tr>
              {columns.map((column, index) => (
                <td 
                  key={ index } 
                  className={'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border'}
                >
                    { column.title }
                </td>
              ))}
            </tr>
            {/* Go through the array of form submissions, printing out each object key, 
            and each row based on the number of objects in the array. */}
            { formData.map ((data, index) => (
              <tr key={index}>
                <td id={'connection_title--' + index} className={'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border text-form-text'}>
                  { data.title }
                </td>
                <td id={'connection_type--' + index} className={'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border text-form-text'}>
                  { data.type }
                </td>
                <td id={'connection_comments--' + index} className={'py-4 pl-4 pr-32 border-solid border-2 border-dark-gray-border text-form-text'}>
                  {/* Needs to actually be 'Run Frequency' */}
                  { data.comments }
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Tag>
  ) 
}

ConnectionList.propTypes = {
  formData: PropTypes.array,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

ConnectionList.defaultProps = {
  formData: [{"title": "1"}, {"type": "2"}, {"lastly": "3"}],
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default ConnectionList;
