/**
 * @file DeploymentName.js
 */
import React from 'react';
import PropTypes from 'prop-types';

const DeploymentName = (props) => {
    const {
        tagName: Tag,
        className,
        variant,
        children
    } = props;

    return (
      <div className={`deployment-name deployment-name--${variant} ${className} inline`}>
        <span className="headertext inline md:ml-4 md:p-2 text-green">Customer Deployment Name</span>
      </div>
    )
}

DeploymentName.propTypes = {
    tagName: PropTypes.string,
    className: PropTypes.string,
    variant: PropTypes.oneOf(['default']),
    children: PropTypes.node
  };
    
  DeploymentName.defaultProps = {
    tagName: 'div',
    className: '',
    variant: 'default',
    children: ''
  }

export default DeploymentName