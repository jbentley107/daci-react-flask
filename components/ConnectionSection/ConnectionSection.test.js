/**
 * @file ConnectionSection.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import ConnectionSection from './ConnectionSection.jsx'

describe('components', () => {
  describe('ConnectionSection', () => {
    it('should mount with props', function () {
      const wrap = mount(<ConnectionSection>Hello World</ConnectionSection>)

      const expectedProps = {
        children: 'Hello World',
        tagName: 'div',
        className: '',
        variant: 'default'
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<ConnectionSection>Hello World</ConnectionSection>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('div')
      // contain styles
      expect(wrap[0].attribs.class).toContain('connection-section')

      // contain text
      expect(wrap[0].children[0].type).toBe('tag')
    })
  })
})