/**
 * @url ConnectionSection.js
 */
import React from 'react';
import PropTypes from 'prop-types';

// Import components
import AddConnectionForm from '../AddConnectionForm/AddConnectionForm'

const ConnectionSection = (props) => {
  const { 
    tagName: Tag,
    className,
    variant,
    children
  } = props;  

  return (
    <div className={`connection-section connection-section--${variant} ${className} md:w-30 md:h-auto border-solid border-standard-border bg-container-background`}>
      <AddConnectionForm />
    </div>
  )
}

ConnectionSection.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
};
  
ConnectionSection.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default ConnectionSection