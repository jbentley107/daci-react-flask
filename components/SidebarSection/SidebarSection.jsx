/**
 * @file SidebarSection.js
 */ 
import React from 'react';
import PropTypes from 'prop-types';

// Import components
import NavigationListing from '../NavigationListing/NavigationListing'

const SidebarSection = (props) => {
    const {
        tagName: Tag,
        className,
        variant,
        children,
    } = props;

    return (
        <div className={`sidebar-section sidebar-section--${variant} ${className} flex-initial w-1/5`}>
            <NavigationListing 
                style={''}
            />
        </div>
    );
};

SidebarSection.propTypes = {
    tagName: PropTypes.string,
    className: PropTypes.string,
    variant: PropTypes.oneOf(['default']),
    children: PropTypes.node
}

SidebarSection.defaultProps = {
    tagName: 'div',
    className: '',
    variant: 'default',
    children: ''
}

export default SidebarSection;