/**
* @file MainPanelSection.js
*/ 
import React from 'react';
import PropTypes from 'prop-types';

// Import Components
import ConnectionSection from '../ConnectionSection/ConnectionSection'

const MainPanelSection = (props) => {
    const { 
        panelName,
        panelActivityName,
        tagName: Tag,
        className,
        variant,
        children
    } = props;

    return (
        <div className={`main-panel-section main-panel-section--${variant} ${className} w-100`}>
            <h2 className="text-xl pb-10"><strong className="font-bold">{panelName}</strong> | {panelActivityName}</h2>
            <ConnectionSection />
        </div>
    )
}

MainPanelSection.propTypes = {
  panelName: PropTypes.string,
  panelActivity: PropTypes.string,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
};
  
MainPanelSection.defaultProps = {
  panelName: 'Headings',
  panelActivityName: 'Add Heading',
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default MainPanelSection;