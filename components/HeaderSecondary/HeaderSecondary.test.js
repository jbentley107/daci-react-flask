/**
 * @file HeaderSecondary.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import HeaderSecondary from './HeaderSecondary.jsx'

describe('components', () => {
  describe('HeaderSecondary', () => {
    it('should mount with props', function () {
      const wrap = mount(<HeaderSecondary>Hello World</HeaderSecondary>)

      const expectedProps = {
        tagName: 'div',
        className: '',
        variant: 'default',
        children: 'Hello World'
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<HeaderSecondary>Hello World</HeaderSecondary>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('div')
      // contain styles
      expect(wrap[0].attribs.class).toContain('header-secondary')

      // contain text
      expect(wrap[0].children[0].type).toBe('tag')
    })
  })
})