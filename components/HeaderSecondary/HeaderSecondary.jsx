/**
 * @file HeaderSecondary.js
 */
import React from 'react';
import PropTypes from 'prop-types';

import PanelTitle from "../PanelTitle/PanelTitle";

const HeaderSecondary = (props) => {
    const {
        tagName: Tag,
        className,
        variant,
        children
      } = props;

    return (
        <div className={`header-secondary header-secondary--${variant} ${className}`}>
            <PanelTitle 
                panelName={'EDIT | CUSTOMER CORE'}
            />
        </div>
    )
}

HeaderSecondary.propTypes = {
    tagName: PropTypes.string,
    className: PropTypes.string,
    variant: PropTypes.oneOf(['default']),
    children: PropTypes.node
  };
    
  HeaderSecondary.defaultProps = {
    tagName: 'div',
    className: '',
    variant: 'default',
    children: ''
  }

export default HeaderSecondary;