import React from 'react';
import Logo from './Logo';

export default {
  title: 'Logo',
  component: Logo,
};

export const NTTLogo = () => <Logo logoImageSrc={'../../../static/ntt_logo.png'}/>;

NTTLogo.story = {
  name: 'using NTT Logo',
};
