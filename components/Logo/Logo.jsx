/**
* @file Logo.js
*/ 
import React from 'react';
import PropTypes from 'prop-types';

const Logo = (props) => {
  const {
    logoImageSrc,
    tagName: Tag,
    className,
    variant,
    children
  } = props;

    return (
        <img className={`logo logo--${variant} ${className} align-middle inline w-20 inline`} src={logoImageSrc} alt="" />
    );
}

Logo.propTypes = {
    logoImageSrc: PropTypes.string,
    tagName: PropTypes.string,
    className: PropTypes.string,
    variant: PropTypes.oneOf(['default']),
    children: PropTypes.node
  };
    
  Logo.defaultProps = {
    logoImageSrc: '../../../static/ntt_logo.png',
    tagName: 'img',
    className: '',
    variant: 'default',
    children: ''
  }

export default Logo;