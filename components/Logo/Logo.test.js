/**
 * @file Logo.test.js
 * @url https://devhints.io/enzyme
 * @url https://github.com/airbnb/enzyme/blob/master/docs/guides/jest.md
 */
import * as React from 'react'
import { shallow, mount, render } from 'enzyme'
import Logo from './Logo.jsx'

describe('components', () => {
  describe('Logo', () => {
    it('should mount with props', function () {
      const wrap = mount(<Logo>Hello World</Logo>)

      const expectedProps = {
        tagName: 'img',
        className: '',
        variant: 'default',
        children: 'Hello World',
        logoImageSrc: '../../../static/ntt_logo.png',
      }

      expect(wrap.props()).toEqual(expectedProps)
    })

    it('should render as type and with content', function () {
      const wrap = render(<Logo>Hello World</Logo>)

      expect(wrap[0].type).toEqual('tag')
      expect(wrap[0].name).toEqual('img')
      // contain styles
      expect(wrap[0].attribs.class).toContain('logo')
    })
  })
})